﻿namespace LigaMOL3.Forms
{
	partial class FMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
			this.MenuP = new System.Windows.Forms.Panel();
			this.SeasonL = new System.Windows.Forms.Label();
			this.MatchesP = new System.Windows.Forms.Panel();
			this.StandingsL = new System.Windows.Forms.Label();
			this.MatchesL = new System.Windows.Forms.Label();
			this.StandingsP = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// MenuP
			// 
			this.MenuP.Location = new System.Drawing.Point(12, 21);
			this.MenuP.Name = "MenuP";
			this.MenuP.Size = new System.Drawing.Size(200, 60);
			this.MenuP.TabIndex = 0;
			// 
			// SeasonL
			// 
			this.SeasonL.AutoSize = true;
			this.SeasonL.Font = new System.Drawing.Font("Andika", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SeasonL.Location = new System.Drawing.Point(47, 135);
			this.SeasonL.Name = "SeasonL";
			this.SeasonL.Size = new System.Drawing.Size(135, 52);
			this.SeasonL.TabIndex = 1;
			this.SeasonL.Text = "Season";
			// 
			// MatchesP
			// 
			this.MatchesP.AutoScroll = true;
			this.MatchesP.Location = new System.Drawing.Point(384, 234);
			this.MatchesP.Name = "MatchesP";
			this.MatchesP.Size = new System.Drawing.Size(200, 100);
			this.MatchesP.TabIndex = 2;
			// 
			// StandingsL
			// 
			this.StandingsL.AutoSize = true;
			this.StandingsL.Font = new System.Drawing.Font("Andika", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StandingsL.Location = new System.Drawing.Point(74, 185);
			this.StandingsL.Name = "StandingsL";
			this.StandingsL.Size = new System.Drawing.Size(150, 44);
			this.StandingsL.TabIndex = 3;
			this.StandingsL.Text = "Standings";
			// 
			// MatchesL
			// 
			this.MatchesL.AutoSize = true;
			this.MatchesL.Font = new System.Drawing.Font("Andika", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MatchesL.Location = new System.Drawing.Point(376, 185);
			this.MatchesL.Name = "MatchesL";
			this.MatchesL.Size = new System.Drawing.Size(131, 44);
			this.MatchesL.TabIndex = 4;
			this.MatchesL.Text = "Matches";
			// 
			// StandingsP
			// 
			this.StandingsP.Location = new System.Drawing.Point(71, 276);
			this.StandingsP.Name = "StandingsP";
			this.StandingsP.Size = new System.Drawing.Size(200, 100);
			this.StandingsP.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(388, 335);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(295, 29);
			this.label1.TabIndex = 5;
			this.label1.Text = "Test label 1234567890.- ăîâșț";
			this.label1.Visible = false;
			// 
			// FMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(884, 482);
			this.ControlBox = false;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.StandingsP);
			this.Controls.Add(this.MatchesL);
			this.Controls.Add(this.StandingsL);
			this.Controls.Add(this.MatchesP);
			this.Controls.Add(this.SeasonL);
			this.Controls.Add(this.MenuP);
			this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FMain";
			this.Text = "Liga MOL";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.FMain_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel MenuP;
		private System.Windows.Forms.Label SeasonL;
		private System.Windows.Forms.Panel MatchesP;
		private System.Windows.Forms.Label StandingsL;
		private System.Windows.Forms.Label MatchesL;
		public System.Windows.Forms.Panel StandingsP;
		private System.Windows.Forms.Label label1;
	}
}