﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LigaMOL3.Classes;
using LigaMOL3.Forms;

namespace LigaMOL3.Forms
{
	public partial class FMain : Form
	{
		private static List<string> MenuButtonCaptions = new List<string>(new string[6] { "Button A", "Button B", "Button C", "Button D", "Button E", "EXIT" });

		public const int ControlPadding = 16;
		public const int StandingsWidth = 970 - ControlPadding;

		public MatchesBox Matches;
		public StandingsTableBox StandingsTable;

		public FMain()
		{
			InitializeComponent();
			MyGuiComponents.FormatFormControls(this);
		}

		private void FMain_Load(object sender, EventArgs e)
		{
			MenuP.SetBounds(ControlPadding, ControlPadding, this.Width - 2 * ControlPadding, 60);
			SeasonL.Location = new Point(MenuP.Left, MenuP.Bottom);
			StandingsL.Location = new Point(SeasonL.Left, SeasonL.Bottom);
			StandingsP.SetBounds(StandingsL.Left, StandingsL.Bottom, StandingsWidth, this.Height - StandingsL.Bottom - ControlPadding);
			MatchesL.Location = new Point(StandingsWidth + 2 * ControlPadding, StandingsL.Top);
			MatchesP.SetBounds(MatchesL.Left, MatchesL.Bottom, this.Width - MatchesL.Left , this.Height - MatchesL.Bottom - ControlPadding);
			MatchesP.AutoScroll = true;

			MyGuiComponents.CreateMenuButtons(MenuP, MenuButtonCaptions, true, MenuButton_Click);
			AppHQ.CurrSeason = AppHQ.Database.Seasons.Last();
			StandingsTable = new StandingsTableBox(AppHQ.CurrSeason.Teams, StandingsP, StandingsTable_Click);
			Matches = new MatchesBox(MatchesP, AppHQ.CurrSeason.RSMatches, AppHQ.CurrSeason.RSDates, MatchButton_Click);
			RefreshInformation();
		}

		///
		///
		///

		public void RefreshInformation()
		{
			SeasonL.Text = "SEASON " + AppHQ.CurrSeason.Name;

			AppHQ.CurrSeason.SortMatchesAndRecreateDateList();
			Matches.RecreateImage();

			AppHQ.CurrSeason.RecreateStandings();
			for (int i = 0; i < AppHQ.CurrSeason.Teams.Count; i++)
			{
				StandingsTable.Teams[i].Team = AppHQ.CurrSeason.Teams[i];
				StandingsTableTeam.DrawStandingsTableTeam(StandingsTable.Teams[i], -1);
			}
		}

		///
		///
		///

		private void MenuButton_Click(object sender, EventArgs e)
		{
			int b = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (b)
			{
				case 5: // Exit
					Application.Exit();
					break;
			}
		}

		private void MatchButton_Click(object sender, EventArgs e)
		{
			MessageBox.Show("MatchButton_Click");
		}

		private void StandingsTable_Click(object sender, EventArgs e)
		{
			MessageBox.Show("StandingsTable_Click");
		}

		/// 
		/// 
		/// 
	}
}
