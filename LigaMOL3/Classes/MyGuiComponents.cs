﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMOL3.Classes
{
	public static class MyGuiComponents
	{
		public static Size SimpleMatchButtonSize = new Size(334, 30);

		public static Color FormBackgroundC = ColorTranslator.FromHtml("#121212");
		public static Color ButtonC = ColorTranslator.FromHtml("#121212");
		public static Color ButtonSelectedC = ColorTranslator.FromHtml("#1F1F1E");
		public static Color FontC = ColorTranslator.FromHtml("#FFFFFF");
		public static Color FontSelectedC = ColorTranslator.FromHtml("#FFFFFF");

		public static Color TableHeaderC = ColorTranslator.FromHtml("#51A60C");
		public static Color TableHeaderFontC = ColorTranslator.FromHtml("#EEEEEE");

		public static Color TableBackgroundC = ColorTranslator.FromHtml("#121212");
		public static Color TableBackgroundSelectedC = ColorTranslator.FromHtml("#1F1F1E");
		public static Color TableFontC = ColorTranslator.FromHtml("#EEEEEE");

		public static Color DateItemBackgroundC = ColorTranslator.FromHtml("#51A60C");
		public static Color DateItemBackgroundSelectedC = ColorTranslator.FromHtml("#51A600");
		public static Color DateItemFontC = ColorTranslator.FromHtml("#EFEFEF");

		public static Color MatchItemBackgroundC = ColorTranslator.FromHtml("#121212");
		public static Color MatchItemBackgroundSelectedC = ColorTranslator.FromHtml("#1F1F1E");
		public static Color HourFontC = ColorTranslator.FromHtml("#9499A1");
		public static Color ScoreFontC = ColorTranslator.FromHtml("#FA57B6");
		public static Color TeamFontC = ColorTranslator.FromHtml("#EFEFEF");

		public static Font GetFont(string name, int size, bool bold)
		{
			return new Font(name, size, bold ? FontStyle.Bold : FontStyle.Regular);
		}

		public static void FormatFormControls(Form form)
		{
			form.BackColor = FormBackgroundC;
			foreach (Control control in form.Controls)
			{
				control.BackColor = FormBackgroundC;
				control.ForeColor = FontC;
			}
		}

		public static List<PictureBoxButton> CreateMenuButtons(Panel container, List<string> captions, bool horizontal, EventHandler click)
		{
			List<PictureBoxButton> result = new List<PictureBoxButton>();
			for (int i = 0, n = captions.Count, dim = horizontal ? container.Width / n : container.Height / n; i < n; i++)
			{
				PictureBoxButton pic = new PictureBoxButton(captions[i], click);
				pic.Parent = container;
				pic.Cursor = Cursors.Hand;
				if (horizontal)
					pic.SetBounds(i * dim, 0, dim, container.Height);
				else
					pic.SetBounds(0, i * dim, container.Width, dim);
				PictureBoxButton.OnMouseLeave(pic, null);
				result.Add(pic);
			}
			return result;
		}
	}

	/// 
	/// 
	/// 

	public class PictureBoxButton : PictureBox
	{
		public string Information;

		public PictureBoxButton(string Information, EventHandler Click)
			: this(Information, Click, OnMouseEnter, OnMouseLeave)
		{
		}

		public PictureBoxButton(string Information, EventHandler Click, EventHandler MouseEnter, EventHandler MouseLeave)
			: base()
		{
			this.Information = Information;
			this.Click += Click;
			this.MouseEnter += MouseEnter;
			this.MouseLeave += MouseLeave;
			MouseLeave(this, null);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, true);
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, false);
		}

		private static void DrawPictureBoxButton(PictureBoxButton pic, bool selected)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush bgBrush = selected ? new SolidBrush(MyGuiComponents.ButtonSelectedC) : new SolidBrush(MyGuiComponents.ButtonC);
			Brush textBrush = selected ? new SolidBrush(MyGuiComponents.FontC) : new SolidBrush(MyGuiComponents.FontSelectedC);
			Font font = MyGuiComponents.GetFont("Segoe UI", 17, true);
			g.FillRectangle(bgBrush, 0, 0, pic.Width, pic.Height);
			if (selected)
				g.DrawRectangle(new Pen(Color.WhiteSmoke), 0, 0, pic.Width - 1, pic.Height - 1);
			Size size = g.MeasureString(pic.Information, font).ToSize();
			g.DrawString(pic.Information, font, textBrush, new Point(pic.Width / 2 - size.Width / 2, pic.Height / 2 - size.Height / 2));
			pic.Image = bmp;
		}
	}

	///
	///
	///

	public class StandingsTableHeader : PictureBox
	{
		private static Brush bgBrush = new SolidBrush(MyGuiComponents.TableHeaderC);
		private static Brush textBrush = new SolidBrush(MyGuiComponents.TableHeaderFontC);
		private static Font font = MyGuiComponents.GetFont("Segoe UI", 18, true);

		public StandingsTableHeader(Panel container, Rectangle bounds)
			: base()
		{
			this.Parent = container;
			this.SetBounds(bounds.Left, bounds.Top, bounds.Width, bounds.Height);
		}

		public static void DrawTableHeader(StandingsTableHeader pic)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);

			for (int iCol = 0, lastX = 0; iCol < StandingsTableBox.ColWidths.Length; iCol++)
			{
				Rectangle cell = new Rectangle(lastX, 0, StandingsTableBox.GetActualColumnWidth(iCol, pic.Width), pic.Height);
				g.FillRectangle(bgBrush, cell);
				Size size = g.MeasureString(StandingsTableBox.ColCaptions[iCol], font).ToSize();
				g.DrawString(StandingsTableBox.ColCaptions[iCol], font, textBrush, cell.Left + cell.Width / 2 - size.Width / 2, cell.Height / 2 - size.Height / 2);
				lastX += StandingsTableBox.GetActualColumnWidth(iCol, pic.Width);
			}

			pic.Image = bmp;
		}
	}

	public class StandingsTableTeam : PictureBox
	{
		private static Brush bgBrush = new SolidBrush(MyGuiComponents.TableBackgroundC);
		private static Brush bgBrushMO = new SolidBrush(MyGuiComponents.TableBackgroundSelectedC);
		private static Brush textBrush = new SolidBrush(MyGuiComponents.TableFontC);

		private static Font fontDefault = MyGuiComponents.GetFont("Andika", 16, false);
		private static Font fontPosition = MyGuiComponents.GetFont("Andika", 21, true);
		private static Font fontTeam = MyGuiComponents.GetFont("Andika", 18, false);
		private static Font fontGoals = MyGuiComponents.GetFont("Andika", 17, false);
		private static Font fontPoints = MyGuiComponents.GetFont("Andika", 19, true);

		public TSeasonTeam Team;

		public StandingsTableTeam(Panel container, Rectangle bounds, TSeasonTeam Team, EventHandler Click)
			: base()
		{
			this.Team = Team;
			this.Parent = container;
			this.SetBounds(bounds.Left, bounds.Top, bounds.Width, bounds.Height);
			this.Cursor = Cursors.Hand;
			this.Click += Click;
			this.MouseEnter += new EventHandler(OnMouseEnter);
			this.MouseLeave += new EventHandler(OnMouseLeave);
			this.MouseMove += new MouseEventHandler(OnMouseMove);
			DrawStandingsTableTeam(this, -1);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{
			DrawStandingsTableTeam(sender as StandingsTableTeam, Cursor.Position.X - AppHQ.MainForm.StandingsP.Left);
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			DrawStandingsTableTeam(sender as StandingsTableTeam, -1);
		}

		public static void OnMouseMove(object sender, MouseEventArgs e)
		{
			DrawStandingsTableTeam(sender as StandingsTableTeam, e.Location.X);
		}

		public static void DrawStandingsTableTeam(StandingsTableTeam pic, int coordX)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			g.FillRectangle(bgBrush, new Rectangle(0, 0, pic.Width, pic.Height));

			for (int iCol = 0, lastX = 0; iCol < StandingsTableBox.ColWidths.Length; iCol++)
			{
				int cellWidth = StandingsTableBox.GetActualColumnWidth(iCol, pic.Width);
				if (lastX <= coordX && coordX < lastX + cellWidth)
					g.FillRectangle(bgBrushMO, new Rectangle(lastX, 0, cellWidth, pic.Height));

				Font font = fontDefault;
				if (iCol == 0)
					font = fontPosition;
				else if (iCol == 1)
					font = fontTeam;
				else if (iCol == 2 || iCol == 7 || iCol == 8)
					font = fontGoals;
				else if (iCol == 9)
					font = fontPoints;
				string text = pic.Team.GetFormattedStandingsData(iCol);
				Size size = g.MeasureString(text, fontDefault).ToSize();
				Point location = new Point(lastX + cellWidth / 2 - size.Width / 2, pic.Height / 2 - size.Height / 2);
				if (iCol == 1)
				{
					g.DrawImage(pic.Team.LogoSmall, new Point(lastX + 8, pic.Height / 2 - pic.Team.LogoSmall.Height / 2));
					location.X = lastX + pic.Team.LogoSmall.Width + 16;
				}

				g.DrawString(text, font, textBrush, location);

				lastX += cellWidth;
			}

			g.DrawLine(new Pen(MyGuiComponents.TableHeaderC), 0, pic.Height - 1, pic.Width - 6, pic.Height - 1);
			pic.Image = bmp;
		}
	}

	public class StandingsTableBox
	{
		public static int[] ColWidths = new int[10] { 8, 36, 7, 4, 4, 4, 4, 13, 9, 11 };
		public static string[] ColCaptions = new string[10] { "Pos", "Team", "MP", "3p", "2p", "1p", "0p", "Goals", "GDiff", "Points" };

		public int HeaderHeight;
		public int TeamRowHeight;
		public StandingsTableHeader Header;
		public List<StandingsTableTeam> Teams;

		public StandingsTableBox(List<TSeasonTeam> seasonTeams, Panel container, EventHandler Click)
			: base()
		{
			this.TeamRowHeight = container.Height / (seasonTeams.Count + 1);
			this.HeaderHeight = container.Height - this.TeamRowHeight * seasonTeams.Count;

			this.Header = new StandingsTableHeader(container, new Rectangle(0, 0, container.Width, this.HeaderHeight));
			StandingsTableHeader.DrawTableHeader(this.Header);
			this.Teams = new List<StandingsTableTeam>();
			for (int i = 0; i < seasonTeams.Count; i++)
				this.Teams.Add(new StandingsTableTeam(container, new Rectangle(0, this.HeaderHeight + i * this.TeamRowHeight, container.Width, this.TeamRowHeight), seasonTeams[i], Click));
		}

		public static int GetActualColumnWidth(int col, int picWidth)
		{
			return (int) Math.Truncate((double) (picWidth * ColWidths[col]) / 100);
		}
	}

	///
	///
	///

	public class MatchListItem
	{
	}

	public class DateItem : MatchListItem
	{
		public DateTime Date;

		public DateItem(DateTime Date)
		{
			this.Date = Date;
		}
	}

	public class MatchItem : MatchListItem
	{
		public TMatch Match;

		public MatchItem(TMatch Match)
		{
			this.Match = Match;
		}
	}

	public class MatchesBox : PictureBox
	{
		public const int ItemHeight = 24;
		public const int ScrollBarPadding = 20;
		public const int HourScoreWidth = 40;

		private static Brush dateBgBrush = new SolidBrush(MyGuiComponents.DateItemBackgroundC);
		private static Brush dateBgBrushMO = new SolidBrush(MyGuiComponents.DateItemBackgroundSelectedC);
		private static Brush dateTextBrush = new SolidBrush(MyGuiComponents.DateItemFontC);
		private static Font dateFont = MyGuiComponents.GetFont("Andika", 12, true);

		private static Brush matchBgBrush = new SolidBrush(MyGuiComponents.MatchItemBackgroundC);
		private static Brush matchBgBrushMO = new SolidBrush(MyGuiComponents.MatchItemBackgroundSelectedC);
		private static Brush hourBrush = new SolidBrush(MyGuiComponents.HourFontC);
		private static Brush scoreBrush = new SolidBrush(MyGuiComponents.ScoreFontC);
		private static Brush teamBrush = new SolidBrush(MyGuiComponents.TeamFontC);
		private static Font hourFont = MyGuiComponents.GetFont("Andika", 10, false);
		private static Font scoreFont = MyGuiComponents.GetFont("Andika", 10, true);
		private static Font teamFont = MyGuiComponents.GetFont("Andika", 9, false);

		public List<TMatch> Matches;
		public List<DateTime> Dates;
		public List<MatchListItem> MatchList;
		public int LastHighlightedItemIndex;

		public MatchesBox(Panel container, List<TMatch> Matches, List<DateTime> Dates, EventHandler Click)
			: base()
		{
			this.Matches = Matches;
			this.Dates = Dates;
			this.MatchList = new List<MatchListItem>();
			this.LastHighlightedItemIndex = -1;
			this.Parent = container;
			this.Cursor = Cursors.Hand;
			this.Click += Click;
			this.MouseMove += OnMouseMove;
			this.MouseLeave += OnMouseLeave;
		}

		public static void OnMouseMove(object sender, MouseEventArgs e)
		{
			MatchesBox mb = sender as MatchesBox;
			if (mb.LastHighlightedItemIndex != -1)
				mb.RedrawItem(mb.LastHighlightedItemIndex, false);
			int newIndex = e.Location.Y / MatchesBox.ItemHeight;
			mb.RedrawItem(newIndex, true);
			//mb.OnPaint(new PaintEventArgs(Graphics.FromImage(mb.Image), GetRectangleForItemIndex(newIndex, mb.Width)));
			mb.LastHighlightedItemIndex = newIndex;
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			MatchesBox mb = sender as MatchesBox;
			if (mb.LastHighlightedItemIndex != -1)
				mb.RedrawItem(mb.LastHighlightedItemIndex, false);
			mb.LastHighlightedItemIndex = -1;
		}

		public void RecreateImage()
		{
			// Regenerate match list items
			MatchList.Clear();
			int iDate = 0, iMatch = 0;
			MatchList.Add(new DateItem(Dates[0]));
			while (iMatch < Matches.Count)
			{
				if (!Dates[iDate].Date.Equals(Matches[iMatch].When.Date))
					MatchList.Add(new DateItem(Dates[++iDate]));
				MatchList.Add(new MatchItem(Matches[iMatch++]));
			}

			// Recreate image
			this.LastHighlightedItemIndex = -1;
			if (this.Image != null)
				this.Image.Dispose();
			Bitmap bmp = new Bitmap(this.Parent.Width - ScrollBarPadding, ItemHeight * MatchList.Count);
			this.SetBounds(0, 0, bmp.Width, bmp.Height);
			//MessageBox.Show(this.Bounds + "\n" + (double) (bmp.Width * bmp.Height) / 1000000 + " MP");
			this.Image = bmp;

			for (int i = 0; i < MatchList.Count; i++)
				RedrawItem(i, false);
		}

		public void RedrawItem(int index, bool highlighted)
		{
			Rectangle rect = GetRectangleForItemIndex(index, this.Width);
			Graphics g = Graphics.FromImage(this.Image);
			if (MatchList[index] is DateItem)
			{
				g.FillRectangle(highlighted ? dateBgBrushMO : dateBgBrush, rect);
				DrawText(g, dateFont, dateTextBrush, rect, (MatchList[index] as DateItem).Date.ToString("dddd, d MMMM yyyy"), 1);
			}
			else
			{
				g.FillRectangle(highlighted ? matchBgBrushMO : matchBgBrush, rect);
				TMatch match = (MatchList[index] as MatchItem).Match;
				DrawText(g, hourFont, hourBrush, new Rectangle(0, rect.Top, HourScoreWidth, rect.Height), match.When.ToString("HH:mm"), 3);
				int w = (rect.Width - 2 * HourScoreWidth) / 2;
				DrawText(g, teamFont, teamBrush, new Rectangle(HourScoreWidth, rect.Top, w, rect.Height), match.HomeTeam.ShortName, 3);
				DrawText(g, scoreFont, scoreBrush, new Rectangle(HourScoreWidth + w, rect.Top, HourScoreWidth, rect.Height), match.Result.FinalScore().ToString(), 2);
				DrawText(g, teamFont, teamBrush, new Rectangle(2 * HourScoreWidth + w, rect.Top, w, rect.Height), match.AwayTeam.ShortName, 1);
			}
			//if (highlighted)
			//	MessageBox.Show(index + "\n" + rect);
		}

		private void DrawText(Graphics g, Font font, Brush brush, Rectangle rect, string text, int alignment)
		{
			Size size = g.MeasureString(text, font).ToSize();
			int x;
			if (alignment == 1) //left
				x = rect.Left;
			else if (alignment == 2) //center
				x = rect.Left + rect.Width / 2 - size.Width / 2;
			else //right
				x = rect.Left + rect.Width - size.Width;
			g.DrawString(text, font, brush, new Point(x, rect.Top + rect.Height / 2 - size.Height / 2));
			//g.DrawRectangle(new Pen(Color.Gray), rect);
		}

		public static Rectangle GetRectangleForItemIndex(int index, int width)
		{
			return new Rectangle(0, index * ItemHeight, width - ScrollBarPadding, ItemHeight);
		}
	}
}
