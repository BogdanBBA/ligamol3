﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LigaMOL3.Classes
{
	public class TDatabase
	{
		public List<TCountry> Countries;
		public List<TCity> Cities;
		public List<TIceRink> IceRinks;
		public List<TTeam> Teams;
		public List<TSeason> Seasons;

		public TDatabase()
		{
			this.Countries = new List<TCountry>();
			this.Cities = new List<TCity>();
			this.IceRinks = new List<TIceRink>();
			this.Teams = new List<TTeam>();
			this.Seasons = new List<TSeason>();
		}

		public TCountry GetCountryByID(string ID, bool NotifyNotFound)
		{
			foreach (TCountry country in Countries)
				if (country.ID.Equals(ID))
					return country;
			if (NotifyNotFound)
				MessageBox.Show("Country with ID=\"" + ID + "\" not found!", "GetCountryByID() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			return null;
		}

		public TCity GetCityByID(string ID, bool NotifyNotFound)
		{
			foreach (TCity city in Cities)
				if (city.ID.Equals(ID))
					return city;
			if (NotifyNotFound)
				MessageBox.Show("City with ID=\"" + ID + "\" not found!", "GetCityByID() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			return null;
		}

		public TIceRink GetIceRinkByID(string ID, bool NotifyNotFound)
		{
			foreach (TIceRink iceRink in IceRinks)
				if (iceRink.ID.Equals(ID))
					return iceRink;
			if (NotifyNotFound)
				MessageBox.Show("Ice rink with ID=\"" + ID + "\" not found!", "GetIceRinkByID() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			return null;
		}

		public TTeam GetTeamByID(string ID, bool NotifyNotFound)
		{
			foreach (TTeam team in Teams)
				if (team.ID.Equals(ID))
					return team;
			if (NotifyNotFound)
				MessageBox.Show("Team with ID=\"" + ID + "\" not found!", "GetTeamByID() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			return null;
		}
	}

	///

	public class TCountry
	{
		public string ID;
		public string Name;
		public Image FlagLarge;
		public Image FlagSmall;

		public TCountry(string ID, string Name)
		{
			this.ID = ID;
			this.Name = Name;
		}

		public override string ToString()
		{
			return string.Format("TCountry[ID={0}, Name={1}]", ID, Name);
		}
	}

	///

	public class TCity
	{
		public string ID;
		public string Name;
		public TCountry Country;
		public Image FlagLarge;
		public Image FlagSmall;

		public TCity(string ID, string Name, TCountry Country)
		{
			this.ID = ID;
			this.Name = Name;
			this.Country = Country;
		}

		public override string ToString()
		{
			return string.Format("TCountry[ID={0}, Name={1}, Country={2}]", ID, Name, Country == null ? "NULL" : Country.ID);
		}
	}

	///

	public class TIceRink
	{
		public string ID;
		public string Name;
		public TCity City;
		public int Built;
		public int Capacity;

		public TIceRink(string ID, string Name, TCity City, int Built, int Capacity)
		{
			this.ID = ID;
			this.Name = Name;
			this.City = City;
			this.Built = Built;
			this.Capacity = Capacity;
		}

		public override string ToString()
		{
			return string.Format("TCountry[ID={0}, Name={1}, City={2}, Built={3}, Capacity={4}]", ID, Name, City == null ? "NULL" : City.ID, Built, Capacity);
		}
	}

	///

	public class TTeam
	{
		public string ID;
		public TCity City;
		public string ShortName;
		public string FullName;
		public string WebpageName;
		public Point MapCoords;
		public List<Color> Colors;
		public Image LogoLarge;
		public Image LogoSmall;

		public TTeam(string ID, TCity City, string ShortName, string FullName, string WebpageName, Point MapCoords, List<Color> Colors, Image LogoLarge, Image LogoSmall)
		{
			this.ID = ID;
			this.City = City;
			this.ShortName = ShortName;
			this.FullName = FullName;
			this.WebpageName = WebpageName;
			this.MapCoords = MapCoords;
			this.Colors = Colors;
			this.LogoLarge = LogoLarge;
			this.LogoSmall = LogoSmall;
		}

		public override string ToString()
		{
			return string.Format("TCountry[ID={0}, ShortName={1}, FullName={2}, WebpageName={3}, city={4}, MapCoord={5}, Colors={6}]",
				ID, ShortName, FullName, WebpageName, City == null ? "NULL" : City.ID, Utils.EncodeCoordinate(MapCoords), Utils.EncodeColorList(Colors));
		}
	}

	///

	public class TSeasonTeam : TTeam
	{
		public TStandingsData StandingsData;

		public TSeasonTeam(string ID, TCity City, string ShortName, string FullName, string WebpageName, Point MapCoords, List<Color> Colors, Image LogoLarge, Image LogoSmall)
			: base(ID, City, ShortName, FullName, WebpageName, MapCoords, Colors, LogoLarge, LogoSmall)
		{
			this.StandingsData = new TStandingsData();
		}

		public TSeasonTeam(TTeam team)
			: base(team.ID, team.City, team.ShortName, team.FullName, team.WebpageName, team.MapCoords, team.Colors, team.LogoLarge, team.LogoSmall)
		{
			this.StandingsData = new TStandingsData();
		}

		public string GetFormattedStandingsData(int col)
		{
			switch (col)
			{
				case 0:
					return this.StandingsData.Position + ".";
				case 1:
					return this.ShortName;
				case 2:
					return this.StandingsData.Played.ToString();
				case 3:
					return this.StandingsData.Points3.ToString();
				case 4:
					return this.StandingsData.Points2.ToString();
				case 5:
					return this.StandingsData.Points1.ToString();
				case 6:
					return this.StandingsData.Points0.ToString();
				case 7:
					return this.StandingsData.GoalsFor + "-" + this.StandingsData.GoalsAgainst;
				case 8:
					return this.StandingsData.GoalDifference <= 0 ? this.StandingsData.GoalDifference.ToString() : "+" + this.StandingsData.GoalDifference;
				case 9:
					return this.StandingsData.Points.ToString();
			}
			return "???";
		}
	}

	///

	public class TPeriodResult
	{
		public int HomeTeamGoals;
		public int AwayTeamGoals;

		public TPeriodResult() : this(0, 0) { }

		public TPeriodResult(int HomeTeamGoals, int AwayTeamGoals)
		{
			this.HomeTeamGoals = HomeTeamGoals < 0 ? -HomeTeamGoals : HomeTeamGoals;
			this.AwayTeamGoals = AwayTeamGoals < 0 ? -AwayTeamGoals : AwayTeamGoals;
		}

		public void AddGoals(int HomeTeamGoals, int AwayTeamGoals)
		{
			this.HomeTeamGoals += HomeTeamGoals;
			this.AwayTeamGoals += AwayTeamGoals;
		}

		public void AddGoals(TPeriodResult periodResult)
		{
			AddGoals(periodResult.HomeTeamGoals, periodResult.AwayTeamGoals);
		}

		public bool HomeTeamWinning()
		{
			return HomeTeamGoals > AwayTeamGoals;
		}

		public bool AwayTeamWinning()
		{
			return HomeTeamGoals < AwayTeamGoals;
		}

		public bool NobodyWinning()
		{
			return HomeTeamGoals == AwayTeamGoals;
		}

		public override string ToString()
		{
			return string.Format("{0}-{1}", HomeTeamGoals, AwayTeamGoals);
		}
	}

	///

	public class TMatchResult
	{
		public List<TPeriodResult> Periods;

		public TMatchResult(string Periods) : this(Utils.DecodePeriodResults(Periods)) { }

		public TMatchResult(List<TPeriodResult> Periods)
		{
			this.Periods = Periods;
		}

		public List<TPeriodResult> GetPeriods(int firstHowMany)
		{
			List<TPeriodResult> result = new List<TPeriodResult>();
			for (int i = 0; i < firstHowMany && i < Periods.Count; i++)
				result.Add(Periods[i]);
			return result;
		}

		public TPeriodResult ResultAfter(int periodCount)
		{
			TPeriodResult result = new TPeriodResult();
			foreach (TPeriodResult period in GetPeriods(periodCount))
				result.AddGoals(period);
			return result;
		}

		public TPeriodResult FinalScore()
		{
			return ResultAfter(Periods.Count);
		}

		public override string ToString()
		{
			return Utils.EncodePeriodResults(this.Periods);
		}
	}

	///

	public class TStandingsData
	{
		public int Position;
		public int Played;
		public int Points3;
		public int Points2;
		public int Points1;
		public int Points0;
		public int GoalsFor;
		public int GoalsAgainst;
		public int GoalDifference;
		public int Points;

		public TStandingsData()
		{
			ResetData();
		}

		public void ResetData()
		{
			Position = 0;
			Played = 0;
			Points3 = 0;
			Points2 = 0;
			Points1 = 0;
			Points0 = 0;
			GoalsFor = 0;
			GoalsAgainst = 0;
			GoalDifference = 0;
			Points = 0;
		}

		public override string ToString()
		{
			return string.Format("{9}. {0} [{1}/{2}/{3}/{4}] {5}-{6} [{7}] {8}p", Played, Points3, Points2, Points1, Points0, GoalsFor, GoalsAgainst, GoalDifference, Points, Position);
		}
	}

	///

	public class TMatch
	{
		public string ID;
		public bool Locked;
		public DateTime When;
		public TTeam HomeTeam;
		public TTeam AwayTeam;
		public bool Played;
		public TMatchResult Result;
		public int Attendance;
		public string GameSheetURL;

		public TMatch(string ID, bool Locked, DateTime When, TTeam HomeTeam, TTeam AwayTeam, bool Played, TMatchResult Result, int Attendance, string GameSheetURL)
		{
			this.ID = ID;
			this.Locked = Locked;
			this.When = When;
			this.HomeTeam = HomeTeam;
			this.AwayTeam = AwayTeam;
			this.Played = Played;
			this.Result = Result;
			this.Attendance = Attendance;
			this.GameSheetURL = GameSheetURL;
		}

		public override string ToString()
		{
			return string.Format("TMatch[ID={0}, Locked={1}, When={2}, HomeTeam={3}, AwayTeam={4}, Played={5}, Result={6}, Attendance={7}, GameSheetURL={8}]",
				ID, Locked, When, HomeTeam == null ? "NULL" : HomeTeam.ID, AwayTeam == null ? "NULL" : AwayTeam.ID, Played, Result, Attendance, GameSheetURL);
		}
	}

	/// 

	public class TSeason
	{
		public string ID;
		public string Name;
		public List<TSeasonTeam> Teams;
		public List<TMatch> RSMatches;
		public List<DateTime> RSDates;
		public List<TMatch> PlayOff;
		public List<DateTime> PlayOffDates;

		public TSeason(string ID, string Name, List<TSeasonTeam> Teams, List<TMatch> RegularSeason, List<TMatch> PlayOff)
		{
			this.ID = ID;
			this.Name = Name;
			this.Teams = Teams;
			this.RSMatches = RegularSeason;
			this.RSDates = new List<DateTime>();
			this.PlayOff = PlayOff;
			this.PlayOffDates = new List<DateTime>();
		}

		public void SortMatchesAndRecreateDateList()
		{
			// Sort matches
			for (int i = 0; i < RSMatches.Count - 1; i++)
				for (int j = i + 1; j < RSMatches.Count; j++)
				{
					TMatch a = RSMatches[i], b = RSMatches[j];
					if (a.When.CompareTo(b.When) > 0)
					{
						TMatch aux = a;
						a = b;
						b = aux;
					}
				}
			// Recreate date list
			RSDates.Clear();
			foreach (TMatch match in RSMatches)
				if (RSDates.IndexOf(match.When.Date) == -1)
					RSDates.Add(match.When.Date);
		}

		public void RecreateStandings()
		{
			// reset
			foreach (TSeasonTeam team in Teams)
				team.StandingsData.ResetData();
			// process match results
			foreach (TMatch match in RSMatches)
			{
				TStandingsData a = GetTeamByID(match.HomeTeam.ID).StandingsData, b = GetTeamByID(match.AwayTeam.ID).StandingsData;
				if (match.Played)
				{
					TMatchResult r = match.Result;
					TPeriodResult score = r.FinalScore();
					if (r.FinalScore().HomeTeamWinning())
						if (r.Periods.Count == 3)
						{
							a.Points3++;
							b.Points0++;
						}
						else
						{
							a.Points2++;
							b.Points1++;
						}
					else
						if (r.Periods.Count == 3)
						{
							a.Points0++;
							b.Points3++;
						}
						else
						{
							a.Points1++;
							b.Points2++;
						}
					a.GoalsFor += score.HomeTeamGoals;
					a.GoalsAgainst += score.AwayTeamGoals;
					b.GoalsFor += score.AwayTeamGoals;
					b.GoalsAgainst += score.HomeTeamGoals;
				}
			}
			// calculate totals
			foreach (TSeasonTeam team in Teams)
			{
				TStandingsData s = team.StandingsData;
				s.GoalDifference = s.GoalsFor - s.GoalsAgainst;
				s.Played = s.Points0 + s.Points1 + s.Points2 + s.Points3;
				s.Points = s.Points1 + s.Points2 * 2 + s.Points3 * 3;
			}
			// sort
			for (int i = 0; i < Teams.Count - 1; i++)
				for (int j = i + 1; j < Teams.Count; j++)
				{
					TSeasonTeam a = Teams[i], b = Teams[j];
					bool mustSwitch = false;
					if (a.StandingsData.Points != b.StandingsData.Points)
						mustSwitch = a.StandingsData.Points < b.StandingsData.Points;
					else if (a.StandingsData.GoalDifference != b.StandingsData.GoalDifference)
						mustSwitch = a.StandingsData.GoalDifference < b.StandingsData.GoalDifference;
					if (mustSwitch)
					{
						TSeasonTeam aux = Teams[i];
						Teams[i] = Teams[j];
						Teams[j] = aux;
					}
				}
			// finalize 
			for (int i = 0; i < Teams.Count; i++)
				Teams[i].StandingsData.Position = i + 1;
		}

		public int CurrentTeamPosition(string teamID)
		{
			for (int i = 0; i < Teams.Count; i++)
				if (Teams[i].ID.Equals(teamID))
					return i + 1;
			return -1;
		}

		public TSeasonTeam GetTeamByID(string teamID)
		{
			foreach (TSeasonTeam team in Teams)
				if (team.ID.Equals(teamID))
					return team;
			return null;
		}
	}
}
