﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigaMOL3.Classes
{
	public static class Utils
	{
		public static Random RandomNumber = new Random();

		public static int Max(int a, int b)
		{ return a > b ? a : b; }

		public static int Min(int a, int b)
		{ return a < b ? a : b; }

		public static string EncodeCoordinate(Point point)
		{
			return string.Format("{0},{1}", point.X, point.Y);
		}

		public static Point DecodeCoordinate(string encodedPoint)
		{
			try
			{ return new Point(Int32.Parse(encodedPoint.Substring(0, encodedPoint.IndexOf(","))), Int32.Parse(encodedPoint.Substring(encodedPoint.IndexOf(",") + 1))); }
			catch (Exception E)
			{ return new Point(0, 0); }
		}

		public static string EncodeDateTime(DateTime dt)
		{
			return dt.ToString("yyyy-MM-dd HH.mm");
		}

		public static DateTime DecodeDateTime(string encodedDateTime)
		{
			int y = 2000, M = 1, d = 1, h = 0, m = 0;
			try
			{
				y = Int32.Parse(encodedDateTime.Substring(0, 4));
				M = Int32.Parse(encodedDateTime.Substring(5, 2));
				d = Int32.Parse(encodedDateTime.Substring(8, 2));
				h = Int32.Parse(encodedDateTime.Substring(11, 2));
				m = Int32.Parse(encodedDateTime.Substring(14, 2));
			}
			catch (Exception E)
			{ }
			return new DateTime(y, M, d, h, m, 0);
		}

		public static string EncodeColorList(List<Color> colors)
		{
			StringBuilder result = new StringBuilder();
			foreach (Color color in colors)
				result.Append(ColorTranslator.ToHtml(color)).Append(",");
			return result.ToString().Substring(0, result.Length - 1);
		}

		public static List<Color> DecodeColorList(string encodedColors)
		{
			List<Color> result = new List<Color>();
			try
			{
				string[] colors = encodedColors.Split(',');
				foreach (string color in colors)
					result.Add(ColorTranslator.FromHtml(color));
			}
			catch (Exception E)
			{
				if (result.Count == 0)
					result.Add(Color.White);
			}
			return result;
		}

		public static string EncodePeriodResults(List<TPeriodResult> periods)
		{
			StringBuilder result = new StringBuilder();
			foreach (TPeriodResult periodResult in periods)
				result.Append(periodResult).Append(";");
			return result.ToString().Substring(0, result.Length - 1);
		}

		public static List<TPeriodResult> DecodePeriodResults(string encodedPeriods)
		{
			List<TPeriodResult> result = new List<TPeriodResult>();
			try
			{
				string[] periods = encodedPeriods.Split(';');
				foreach (string period in periods)
				{
					int a = Int32.Parse(period.Substring(0, period.IndexOf('-'))), b = Int32.Parse(period.Substring(period.IndexOf('-') + 1));
					result.Add(new TPeriodResult(a, b));
				}
			}
			catch (Exception E)
			{
				for (int i = result.Count; i < 3; i++)
					result.Add(new TPeriodResult());
			}
			return result;
		}

		public static string EncodeTeamList(List<TSeasonTeam> teams)
		{
			StringBuilder result = new StringBuilder();
			foreach (TSeasonTeam team in teams)
				result.Append(team.ID).Append(",");
			return result.ToString().Substring(0, result.Length - 1);
		}

		public static List<TSeasonTeam> DecodeTeamList(string encodedTeams)
		{
			List<TSeasonTeam> result = new List<TSeasonTeam>();
			try
			{
				string[] teams = encodedTeams.Split(',');
				foreach (string team in teams)
					result.Add(new TSeasonTeam(AppHQ.Database.GetTeamByID(team, false)));
			}
			catch (Exception E)
			{
			}
			return result;
		}

		public static TSeasonTeam GetSeasonTeamByIDFromList(string ID, List<TSeasonTeam> list)
		{
			foreach (TSeasonTeam team in list)
				if (team.ID.Equals(ID))
					return team;
			return null;
		}

		public static string ValidateRegularSeasonMatchResult(TMatchResult matchResult)
		{
			try
			{
				if (matchResult.Periods.Count < 3 && matchResult.Periods.Count > 5)
					return "The number of periods must be 3, 4 or 5.";
				switch (matchResult.Periods.Count)
				{
					case 3:
						break;
					case 4:
						if (!matchResult.ResultAfter(3).NobodyWinning())
							return "The match ended after 4 periods, but one of the teams was winning after 3 periods.";
						break;
					case 5:
						if (!matchResult.ResultAfter(3).NobodyWinning())
							return "The match ended after 5 periods, but one of the teams was winning after 3 periods.";
						if (!matchResult.ResultAfter(4).NobodyWinning())
							return "The match ended after 5 periods, but one of the teams was winning after 4 periods.";
						break;
				}
				if (matchResult.FinalScore().NobodyWinning())
					return "The match has " + matchResult.Periods.Count + " periods in total, and the final score is equal.";
			}
			catch (Exception E)
			{
				return E.ToString();
			}
			return "";
		}

		public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
		{
			double ratioX = (double) maxWidth / image.Width;
			double ratioY = (double) maxHeight / image.Height;
			double ratio = Math.Min(ratioX, ratioY);

			int newWidth = (int) (image.Width * ratio);
			int newHeight = (int) (image.Height * ratio);

			Image newImage = new Bitmap(newWidth, newHeight);
			Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
			return newImage;
		}

		public static bool FontIsInstalled(string fontName)
		{
			InstalledFontCollection fontsCollection = new InstalledFontCollection();
			foreach (FontFamily fontFamily in fontsCollection.Families)
				if (fontFamily.Name.Equals(fontName))
					return true;
			return false;
		}
	}

	public static class Paths
	{
		public static string Data = @"data\";
		public static string Fonts = @"data\fonts\";
		public static string Database = @"data\database.xml";
		public static string Settings = @"data\settings.xml";
		public static string Images = @"data\images\";
		public static string Logo = @"data\images\logo.png";
		public static string Map = @"data\images\map.jpg";
		public static string CountryImages = @"data\images\countries\";
		public static string CityImages = @"data\images\cities\";
		public static string TeamImages = @"data\images\teams\";
	}
}
