﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LigaMOL3.Forms;

namespace LigaMOL3.Classes
{
	public static class AppHQ
	{
		public static TDatabase Database = new TDatabase();
		public static TSeason CurrSeason = null;

		public static FMain MainForm;

		public static void CreateForms()
		{
			MainForm = new FMain();
		}
	}
}
