﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LigaMOL3.Classes;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;

namespace LigaMOL3
{
	public partial class FInitialization : Form
	{
		[DllImport("gdi32.dll", EntryPoint = "AddFontResourceW", SetLastError = true)]
		public static extern int AddFontResource([In][MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

		private static string[] LoadingStatusMessages = new string[6] { "Playing matches...", "Cross-checking adversaries...", "Building up rivalries...", "Preparing the ice...", "Printing tickets...", "Warming up..." };
		private int LoadingStatusMessageIndex = 0;

		public FInitialization()
		{
			InitializeComponent();
			MyGuiComponents.FormatFormControls(this);
		}

		private void FInitialization_Shown(object sender, EventArgs e)
		{
			LoadingStatusMessageTimer_Tick(null, null);
			LoadingStatusMessageTimer.Enabled = true;
			InitializationThreadStarter.Enabled = true;
		}

		private void InitializationThreadStarter_Tick(object sender, EventArgs e)
		{
			InitializationThreadStarter.Enabled = false;
			InitializeAppThread.RunWorkerAsync();
		}

		private void LoadingStatusMessageTimer_Tick(object sender, EventArgs e)
		{
			while (LoadingStatusMessages[LoadingStatusMessageIndex].Equals(LoadingStatusMessageLabel.Text))
				LoadingStatusMessageIndex = Utils.RandomNumber.Next(LoadingStatusMessages.Length);
			LoadingStatusMessageLabel.Text = LoadingStatusMessages[LoadingStatusMessageIndex];
		}

		private void InitializeAppThread_DoWork(object sender, DoWorkEventArgs e)
		{
			string phase = "initializing";
			try
			{
				// Checking files/folders

				phase = "checking folders/files";
				if (!Directory.Exists(Paths.Data))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.Data + "\" doesn't exit!");
				if (!Directory.Exists(Paths.Fonts))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.Fonts + "\" doesn't exit!");
				if (!File.Exists(Paths.Database))
					DisplayErrorMessageAndCancelThread("The file \"" + Paths.Database + "\" doesn't exit!");
				if (!File.Exists(Paths.Settings))
					DisplayErrorMessageAndCancelThread("The file \"" + Paths.Settings + "\" doesn't exit!");
				if (!Directory.Exists(Paths.Images))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.Images + "\" doesn't exit!");
				if (!File.Exists(Paths.Logo))
					DisplayErrorMessageAndCancelThread("The file \"" + Paths.Logo + "\" doesn't exit!");
				if (!File.Exists(Paths.Map))
					DisplayErrorMessageAndCancelThread("The file \"" + Paths.Map + "\" doesn't exit!");
				if (!Directory.Exists(Paths.CountryImages))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.CountryImages + "\" doesn't exit!");
				if (!Directory.Exists(Paths.CityImages))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.CityImages + "\" doesn't exit!");
				if (!Directory.Exists(Paths.TeamImages))
					DisplayErrorMessageAndCancelThread("The folder \"" + Paths.TeamImages + "\" doesn't exit!");

				// Load fonts

				foreach (string font in Directory.GetFiles(Paths.Fonts, "*.ttf"))
				{
					//MessageBox.Show(font);
					if (AddFontResource(font) == 0)
						throw new ApplicationException("Font \'" + font + "\" could not be installed!");
				}

				// Load XML database

				phase = "loading xml";
				XmlDocument doc = new XmlDocument();
				doc.Load(Paths.Database);
				phase = "decoding xml database (countries)";
				XmlNodeList nodes = doc.SelectNodes("LigaMOL3_Database/Countries/Country");
				foreach (XmlNode c in nodes)
				{
					TCountry country = new TCountry(c.Attributes["ID"].Value, c.Attributes["name"].Value);
					AppHQ.Database.Countries.Add(country);
				}
				phase = "decoding xml database (cities)";
				nodes = doc.SelectNodes("LigaMOL3_Database/Cities/City");
				foreach (XmlNode c in nodes)
				{
					TCountry country = AppHQ.Database.GetCountryByID(c.Attributes["country"].Value, false);
					TCity city = new TCity(c.Attributes["ID"].Value, c.Attributes["name"].Value, country);
					AppHQ.Database.Cities.Add(city);
				}
				phase = "decoding xml database (ice rinks)";
				nodes = doc.SelectNodes("LigaMOL3_Database/IceRinks/IceRink");
				foreach (XmlNode i in nodes)
				{
					TCity city = AppHQ.Database.GetCityByID(i.Attributes["city"].Value, false);
					int yearBuilt = Int32.Parse(i.Attributes["built"].Value), capacity = Int32.Parse(i.Attributes["capacity"].Value);
					TIceRink iceRink = new TIceRink(i.Attributes["ID"].Value, i.Attributes["name"].Value, city, yearBuilt, capacity);
					AppHQ.Database.IceRinks.Add(iceRink);
				}
				phase = "decoding xml database (teams)";
				nodes = doc.SelectNodes("LigaMOL3_Database/Teams/Team");
				foreach (XmlNode t in nodes)
				{
					string sn = t.Attributes["shortName"].Value, fn = t.Attributes["fullName"].Value, wn = t.Attributes["WPName"].Value;
					TCity city = AppHQ.Database.GetCityByID(t.Attributes["city"].Value, false);
					Point p = Utils.DecodeCoordinate(t.Attributes["mapXY"].Value);
					List<Color> cols = Utils.DecodeColorList(t.Attributes["colors"].Value);
					TTeam team = new TTeam(t.Attributes["ID"].Value, city, sn, fn, wn, p, cols, null, null);
					AppHQ.Database.Teams.Add(team);
				}

				// Checking data integrity

				phase = "checking for empty lists";
				if (AppHQ.Database.Countries.Count == 0)
					DisplayErrorMessageAndCancelThread("There are no countries, WTF ?!");
				if (AppHQ.Database.Cities.Count == 0)
					DisplayErrorMessageAndCancelThread("There are no cities, WTF ?!");
				if (AppHQ.Database.IceRinks.Count == 0)
					DisplayErrorMessageAndCancelThread("There are no ice rinks, WTF ?!");
				if (AppHQ.Database.Teams.Count == 0)
					DisplayErrorMessageAndCancelThread("There are no teams, WTF ?!");

				phase = "checking for null pointers";
				foreach (TCity city in AppHQ.Database.Cities)
					if (city.Country == null)
						DisplayErrorMessageAndCancelThread("Country for city \"" + city.Name + "\" is null !");
				foreach (TIceRink iceRink in AppHQ.Database.IceRinks)
					if (iceRink.City == null)
						DisplayErrorMessageAndCancelThread("City for ice rink \"" + iceRink.Name + "\" is null !");
				foreach (TTeam team in AppHQ.Database.Teams)
					if (team.City == null)
						DisplayErrorMessageAndCancelThread("City for team \"" + team.ShortName + "\" is null !");

				phase = "checking for identical IDs";
				for (int i = 0; i < AppHQ.Database.Countries.Count - 1; i++)
					for (int j = i + 1; j < AppHQ.Database.Countries.Count; j++)
						if (AppHQ.Database.Countries[i].ID.Equals(AppHQ.Database.Countries[j].ID))
							DisplayErrorMessageAndCancelThread("Countries with indexes i=" + i + " and j=" + j + " have identical IDs! ");
				for (int i = 0; i < AppHQ.Database.Cities.Count - 1; i++)
					for (int j = i + 1; j < AppHQ.Database.Cities.Count; j++)
						if (AppHQ.Database.Cities[i].ID.Equals(AppHQ.Database.Cities[j].ID))
							DisplayErrorMessageAndCancelThread("Cities with indexes i=" + i + " and j=" + j + " have identical IDs! ");
				for (int i = 0; i < AppHQ.Database.IceRinks.Count - 1; i++)
					for (int j = i + 1; j < AppHQ.Database.IceRinks.Count; j++)
						if (AppHQ.Database.IceRinks[i].ID.Equals(AppHQ.Database.IceRinks[j].ID))
							DisplayErrorMessageAndCancelThread("Ice rinks with indexes i=" + i + " and j=" + j + " have identical IDs! ");
				for (int i = 0; i < AppHQ.Database.Teams.Count - 1; i++)
					for (int j = i + 1; j < AppHQ.Database.Teams.Count; j++)
						if (AppHQ.Database.Teams[i].ID.Equals(AppHQ.Database.Teams[j].ID))
							DisplayErrorMessageAndCancelThread("Teams with indexes i=" + i + " and j=" + j + " have identical IDs! ");

				// Load country/city/team .. flags/logos

				const int lLarge = 210, lSmall = 70;
				string imgPath;

				phase = "loading country flags";
				foreach (TCountry country in AppHQ.Database.Countries)
				{
					imgPath = Paths.CountryImages + country.ID + ".png";
					if (!File.Exists(imgPath))
						throw new ApplicationException("Country flag \"" + imgPath + "\" does not exist!");
					Image img = new Bitmap(imgPath);
					country.FlagLarge = Utils.ScaleImage(img, lLarge, lLarge);
					country.FlagSmall = Utils.ScaleImage(img, lSmall, lSmall);
					img.Dispose();
				}

				phase = "loading city flags";
				foreach (TCity city in AppHQ.Database.Cities)
				{
					imgPath = Paths.CityImages + city.ID + ".png";
					if (!File.Exists(imgPath))
						throw new ApplicationException("City flag \"" + imgPath + "\" does not exist!");
					Image img = new Bitmap(imgPath);
					city.FlagLarge = Utils.ScaleImage(img, lLarge, lLarge);
					city.FlagSmall = Utils.ScaleImage(img, lSmall, lSmall);
					img.Dispose();
				}

				phase = "loading team logos";
				foreach (TTeam team in AppHQ.Database.Teams)
				{
					imgPath = Paths.TeamImages + team.ID + ".png";
					if (!File.Exists(imgPath))
						throw new ApplicationException("Team logo \"" + imgPath + "\" does not exist!");
					Image img = new Bitmap(imgPath);
					team.LogoLarge = Utils.ScaleImage(img, lLarge, lLarge);
					team.LogoSmall = Utils.ScaleImage(img, lSmall, lSmall);
					img.Dispose();
				}

				// Decoding XML seasons

				phase = "decoding xml seasons";
				nodes = doc.SelectNodes("LigaMOL3_Database/Seasons/Season");
				foreach (XmlNode s in nodes)
				{
					// Getting season info

					phase = "decoding xml season; post-" + phase;
					string seasId = s.Attributes["ID"].Value, seasName = s.Attributes["name"].Value;
					List<TSeasonTeam> teams = Utils.DecodeTeamList(s.Attributes["teams"].Value);
					List<TMatch> regSeason = new List<TMatch>();

					phase = "decoding regular season matches for season " + seasName;
					foreach (XmlNode m in s.SelectNodes("RSMatch"))
					{
						string id = m.Attributes["ID"].Value, matchTeams = m.Attributes["teams"].Value;
						phase = "decoding regular season matches for season " + seasName + ", match id=" + id;
						bool locked = Boolean.Parse(m.Attributes["locked"].Value);
						DateTime when = Utils.DecodeDateTime(m.Attributes["when"].Value);
						TSeasonTeam t1 = Utils.GetSeasonTeamByIDFromList(matchTeams.Substring(0, matchTeams.IndexOf("-")), teams);
						TSeasonTeam t2 = Utils.GetSeasonTeamByIDFromList(matchTeams.Substring(matchTeams.IndexOf("-") + 1), teams);
						bool played = Boolean.Parse(m.Attributes["played"].Value);
						TMatchResult matchResult = new TMatchResult(m.Attributes["result"].Value);
						int attendance = Int32.Parse(m.Attributes["attendance"].Value);
						string html = m.Attributes["sheetHTML"].Value;
						TMatch match = new TMatch(id, locked, when, t1, t2, played, matchResult, attendance, html);
						regSeason.Add(match);
					}

					// Verifying season data integrity

					phase = "checking for empty lists";
					if (teams.Count == 0)
						DisplayErrorMessageAndCancelThread("There are no teams for season " + seasName + ", WTF ?!");
					if (regSeason.Count == 0)
						DisplayErrorMessageAndCancelThread("There are no regular season matches for season " + seasName + ", this will be treated as an error for now. If you see this and don't agree, change the code.");

					phase = "checking for null pointers";
					foreach (TSeasonTeam team in teams)
						if (team == null)
							DisplayErrorMessageAndCancelThread("There is a null pointer for a team for season " + seasName + " !");
					foreach (TMatch match in regSeason)
					{
						if (match == null)
							DisplayErrorMessageAndCancelThread("There is a null pointer for a regular season match for season " + seasName + " !");
						if (match.HomeTeam == null || match.AwayTeam == null)
							DisplayErrorMessageAndCancelThread("There is a null pointer for one of the teams in a regular season match (id=" + match.ID + ") for season " + seasName + " !");
						if (match.Played)
							if (!Utils.ValidateRegularSeasonMatchResult(match.Result).Equals(""))
								DisplayErrorMessageAndCancelThread("The result for the regular season match (id=" + match.ID + ") for season " + seasName + " is incorrect !\n\nError text: " + Utils.ValidateRegularSeasonMatchResult(match.Result) + " The score is " + match.Result + ".");
					}

					phase = "checking for identical IDs";
					for (int i = 0; i < teams.Count - 1; i++)
						for (int j = i + 1; j < teams.Count; j++)
							if (teams[i].ID.Equals(teams[j].ID))
								DisplayErrorMessageAndCancelThread("Duplicate team ID (" + teams[i].ID + ") in season " + seasName + " ! ");
					for (int i = 0; i < regSeason.Count - 1; i++)
						for (int j = i + 1; j < regSeason.Count; j++)
							if (regSeason[i].ID.Equals(regSeason[j].ID))
								DisplayErrorMessageAndCancelThread("Duplicate match ID (" + regSeason[i].ID + ") in season " + seasName + " ! ");


					// Finalizing season

					TSeason season = new TSeason(seasId, seasName, teams, regSeason, null);
					AppHQ.Database.Seasons.Add(season);
				}

				// Creating and initializing other forms

				phase = "initializing other forms";
				AppHQ.CreateForms();
			}
			catch (ApplicationException E)
			{
				DisplayErrorMessageAndCancelThread("An understandable ERROR occured while initializing the application.\n\nPhase: " + phase + "\n\n" + E.ToString());
			}
			catch (Exception E)
			{
				DisplayErrorMessageAndCancelThread("An unchecked/unexpected ERROR occured while initializing the application.\n\nPhase: " + phase + "\n\n" + E.ToString());
			}
		}

		private void DisplayErrorMessageAndCancelThread(string msg)
		{
			InitializeAppThread.CancelAsync();
			MessageBox.Show(msg + "\n\nTry to fix the issues described above and restart the application.", "Initialization error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			Application.Exit();
		}

		private void InitializeAppThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			//
		}

		private void InitializeAppThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			StatusLabel.Text = "Complete :) !";
			LoadingStatusMessageTimer.Enabled = false;
			AppHQ.MainForm.Show();
			this.Hide();
		}
	}
}
