﻿namespace LigaMOL3
{
	partial class FInitialization
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.StatusLabel = new System.Windows.Forms.Label();
			this.LoadingStatusMessageLabel = new System.Windows.Forms.Label();
			this.LoadingStatusMessageTimer = new System.Windows.Forms.Timer(this.components);
			this.InitializeAppThread = new System.ComponentModel.BackgroundWorker();
			this.InitializationThreadStarter = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::LigaMOL3.Properties.Resources.logo;
			this.pictureBox1.Location = new System.Drawing.Point(32, 32);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(200, 200);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// StatusLabel
			// 
			this.StatusLabel.AutoSize = true;
			this.StatusLabel.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusLabel.Location = new System.Drawing.Point(249, 32);
			this.StatusLabel.Name = "StatusLabel";
			this.StatusLabel.Size = new System.Drawing.Size(327, 37);
			this.StatusLabel.TabIndex = 1;
			this.StatusLabel.Text = "Initializing application...";
			// 
			// LoadingStatusMessageLabel
			// 
			this.LoadingStatusMessageLabel.AutoSize = true;
			this.LoadingStatusMessageLabel.Location = new System.Drawing.Point(252, 79);
			this.LoadingStatusMessageLabel.Name = "LoadingStatusMessageLabel";
			this.LoadingStatusMessageLabel.Size = new System.Drawing.Size(132, 21);
			this.LoadingStatusMessageLabel.TabIndex = 2;
			this.LoadingStatusMessageLabel.Text = "Playing matches...";
			// 
			// LoadingStatusMessageTimer
			// 
			this.LoadingStatusMessageTimer.Interval = 2000;
			this.LoadingStatusMessageTimer.Tick += new System.EventHandler(this.LoadingStatusMessageTimer_Tick);
			// 
			// InitializeAppThread
			// 
			this.InitializeAppThread.WorkerReportsProgress = true;
			this.InitializeAppThread.WorkerSupportsCancellation = true;
			this.InitializeAppThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.InitializeAppThread_DoWork);
			this.InitializeAppThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.InitializeAppThread_ProgressChanged);
			this.InitializeAppThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.InitializeAppThread_RunWorkerCompleted);
			// 
			// InitializationThreadStarter
			// 
			this.InitializationThreadStarter.Interval = 200;
			this.InitializationThreadStarter.Tick += new System.EventHandler(this.InitializationThreadStarter_Tick);
			// 
			// FInitialization
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.ClientSize = new System.Drawing.Size(668, 264);
			this.ControlBox = false;
			this.Controls.Add(this.LoadingStatusMessageLabel);
			this.Controls.Add(this.StatusLabel);
			this.Controls.Add(this.pictureBox1);
			this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FInitialization";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Initializing LigaMOL3...";
			this.Shown += new System.EventHandler(this.FInitialization_Shown);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label StatusLabel;
		private System.Windows.Forms.Label LoadingStatusMessageLabel;
		private System.Windows.Forms.Timer LoadingStatusMessageTimer;
		private System.ComponentModel.BackgroundWorker InitializeAppThread;
		private System.Windows.Forms.Timer InitializationThreadStarter;
	}
}

